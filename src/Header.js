import React from 'react';
import './styles/Header.css';

var header = {
    fontFamily: 'sans-serif',
    color: 'red'
};

class Header extends React.Component {
    render () {
        return (
            <header className="website-header">
                <h1 style={header}>My website {this.props.websiteTitle}</h1>
                <span>{this.props.subtitle}</span>
            </header>
        )
    }
}

export default Header;